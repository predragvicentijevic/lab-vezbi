from time import sleep
import random
import math
#import numpy as np
from copy import deepcopy
import pandas as pd
from sklearn.datasets import load_iris
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.naive_bayes import BernoulliNB
from sklearn import tree
from sklearn.datasets import load_wine
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score


BOARD_SIZE = 4
PAWN_ACTIONS = ['a', 'l', 'r']
EMPTY_SQUARE = ' '


def check_victory(board):
    # return True if 'o' in board[0] or 'x' in board[-1] else False
    if 'o' in board[0]:
        return 'o'
    elif 'x' in board[-1]:
        return 'x'
    else:
        return 'n'


def find_possible_moves(board, sign):
    sign_locations = [(i, j) for i, row in enumerate(board) for j, square in enumerate(row) if board[i][j] == sign]
    possible_moves = []
    if sign == 'x':
        for sign_i, sign_j in sign_locations:
            if board[sign_i + 1][sign_j] == EMPTY_SQUARE:
                possible_moves.append((sign_i, sign_j, 'a'))
            if sign_j > 0 and board[sign_i + 1][sign_j - 1] == 'o':
                possible_moves.append((sign_i, sign_j, 'r'))
            if sign_j < BOARD_SIZE - 1 and board[sign_i + 1][sign_j + 1] == 'o':
                possible_moves.append((sign_i, sign_j, 'l'))
    elif sign == 'o':
        for sign_i, sign_j in sign_locations:
            if board[sign_i - 1][sign_j] == EMPTY_SQUARE:
                possible_moves.append((sign_i, sign_j, 'a'))
            if sign_j > 0 and board[sign_i - 1][sign_j - 1] == 'x':
                possible_moves.append((sign_i, sign_j, 'l'))
            if sign_j < BOARD_SIZE - 1 and board[sign_i - 1][sign_j + 1] == 'x':
                possible_moves.append((sign_i, sign_j, 'r'))

    return possible_moves


def make_move(board, move, sign):
    i, j, action = move
    board[i][j] = EMPTY_SQUARE
    if sign == 'x':
        if (i+1<=3 and i+1>=0) and action == 'a' and board[i+1][j]!='x' and board[i+1][j]!='o':
            board[i + 1][j] = 'x'
        elif (i+1<=3 and i+1>=0 and j+1<=3 and j+1>=0) and action == 'l' and board[i+1][j+1]=='o':
            board[i + 1][j + 1] = 'x'
        elif (i+1<=3 and i+1>=0 and j-1<=3 and j-1>=0) and action == 'r' and board[i+1][j-1]=='o':
            board[i + 1][j - 1] = 'x'
    elif sign == 'o':
        if (i-1<=3 and i-1>=0) and action == 'a' and board[i-1][j]!='x' and board[i-1][j]!='o':
            board[i - 1][j] = 'o'
        elif (i-1<=3 and i-1>=0 and j-1<=3 and j-1>=0) and action == 'l' and board[i-1][j-1]=='x':
            board[i - 1][j - 1] = 'o'
        elif (i-1<=3 and i-1>=0 and j+1<=3 and j+1>=0) and action == 'r' and board[i-1][j+1]=='x':
            board[i - 1][j + 1] = 'o'


def next_move(board, my_sign, opponent_sign):
    """
    Тука треба да го вратите следниот потег кој би го одиграле.
    Потег се состои од (локација i на пиунот кој го поместувате, цел број во опсег [0..BOARD_SIZE),
                        локација j на пиунот кој го поместувате, цел број во опсег [0..BOARD_SIZE),
                        тип на поместување - може да биде:
                            'a' - оди напред
                            'l' - преземи пиун лево од тебе
                            'r' - преземи пиун десно од тебе
    На влез добивата табла како листа од листи. Секогаш Играч 1 е 'x', а Играч 2 е 'o'.
    Внимавајте на перспективата: Играч 1 'x' секогаш почнува од горе и напаѓа надолу.
                                 Играч 2 'o' секогаш почнува од доле и напаѓа нагоре.
    Бидејќи скриптата која ја пишувате може некогаш да биде Играч 1, а некогаш Играч 2, ја гледате променливата my_sign
    за да знаете со кој знак играте вие.
    """
    # Gi zemame site mozni potezi od momentalnata sostojba
    possible_moves = find_possible_moves(board, my_sign)
    if possible_moves:
        best_score = math.inf
        best_move = None

        for move in possible_moves:
            
            copy_board = deepcopy(board)
            make_move(copy_board, move, my_sign)
            
            current_score = minimax(copy_board, my_sign)
            # Ako potegot sto go razgleduvame e podobar od momentalniot najdobar poteg
            if current_score < best_score:
                # Toj e sega najdobriot poteg
                best_move = move
                # Negoviot score e sega najdobriot score
                best_score = current_score

        return best_move




def minimax(board, my_sign):

    if check_victory(board) == 'o':
        return -100000
    elif check_victory(board) == 'x':
        return 100000

    if my_sign == 'x':
        max_score = -math.inf
        for move in find_possible_moves(board, my_sign):
            make_move(board, move, my_sign)

            # Od tuka nadole, na red e 'o'
            current_score = minimax(board, 'o')
            max_score = max(current_score, max_score)

        return max_score

    elif my_sign == 'o':
        min_score = math.inf
        for move in find_possible_moves(board, my_sign):
            make_move(board, move, my_sign)

            # Od tuka nadole, na red e 'x'
            current_score = minimax(board, 'x')
            min_score = min(current_score, min_score)

        return min_score
