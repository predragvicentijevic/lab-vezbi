BOARD_SIZE = 4  
PAWN_ACTIONS = ['a', 'l', 'r']
EMPTY_SQUARE = ' '


def read_user_input():
    message = input('Внесете потег: ').split()
    while True:
        try:
            i, j, action = int(message[0]), int(message[1]), message[2]
            if 0 <= i < BOARD_SIZE and 0 <= j < BOARD_SIZE and action in PAWN_ACTIONS:
                return i, j, action
            message = input('Лоша синтакса. pawn_i pawn_j pawn_action: ').split()
        except (IndexError, ValueError):
            message = input('Лоша синтакса. pawn_i pawn_j pawn_action: ').split()


def next_move1(board, my_sign, opponent_sign):
    return read_user_input()

def next_move(board, my_sign, opponent_sign):
    while True:
        i, j , poteg = read_user_input()
        check = False
        if (i+1<=3 and i+1>=0) and board[i+1][j]!='o' and board[i+1][j]!='x' and poteg=='a':
            check = True
        elif (i+1<=3 and i+1>=0 and j+1<=3 and j+1>=0) and board[i+1][j+1]=='o' and poteg=='l':
            check = True
        elif (i+1<=3 and i+1>=0 and j-1<=3 and j-1>=0) and board[i+1][j-1]=='o' and poteg=='r':
            check = True
        if check == True:
            return (i,j,poteg)
        else:
            print('invalid move')