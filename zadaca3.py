import pandas as pd
import numpy as np
from sklearn.datasets import load_wine
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

data = load_wine()
X = data['data']
Y = data['target']
klasa = data['target_names']
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, random_state = 155, test_size = 0.25)

clf = MLPClassifier(max_iter=10000)
clf.fit(X_train, Y_train)
Y_pred =clf.predict(X_test)
print('Accuracy Score on train data: ', accuracy_score(y_true=Y_train, y_pred=clf.predict(X_train)))
print('Accuracy Score on test data: ', accuracy_score(y_true=Y_test, y_pred=Y_pred))

loza = [X[51]]
print(klasa[clf.predict(loza)])
