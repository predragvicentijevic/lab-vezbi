from collections import deque
import itertools

def unpack_state(state):
    boat = state[0]
    left_bank = set([passengers[index] for index, side in enumerate(state[1:]) if side == 'left'])
    right_bank = set([passengers[index] for index, side in enumerate(state[1:]) if side == 'right'])
    return boat, left_bank, right_bank

def search_path(initial_state, goal_state):
    visited = {initial_state}
    states_queue = deque([[initial_state]])
    while states_queue:
        states_list = states_queue.popleft()
        state_to_expand = states_list[-1]
        for next_state in expand_state(state_to_expand):
            if next_state not in visited:
                if next_state == goal_state:
                    return states_list + [next_state]
                visited.add(next_state)
                states_queue.append(states_list + [next_state])
    return []

def visualise(path):
    """
    Function to visualise path returned from the search function
    :param path: path to be visualised
    :return: None
    """
    if not path:
        print('Search path did not find a solution')
        return
    for pair_of_states in zip(path, path[1:]):
        boat_old, left_old, right_old = unpack_state(pair_of_states[0])
        boat_new, left_new, right_new = unpack_state(pair_of_states[1])
        delimiter_space = ' ' * 50
        separated_print(left_old)
        print(delimiter_space, end='')
        separated_print(right_old)
        print()
        if boat_old == 'left':
            delimiter = ' ' * 5 + '>' * 15 + ' ' * 5
            separated_print(left_new)
            print(delimiter, end='')
            separated_print(left_old - left_new)
            print(delimiter, end='')
            separated_print(right_old)
            print()
        else:
            delimiter = ' ' * 5 + '<' * 15 + ' ' * 5
            separated_print(left_old)
            print(delimiter, end='')
            separated_print(right_old - right_new)
            print(delimiter, end='')
            separated_print(right_new)
            print()
        separated_print(left_new)
        print(delimiter_space, end='')
        separated_print(right_new)
        print()
        print()
        print()

def separated_print(iterable):
    for element in iterable:
        print(element, end=' ')
    if not iterable:
        print('Empty', end='')

def expand_state(state):
    boat, left_bank, right_bank = unpack_state(state)
    if boat == 'left':
        boat_trips = BoatTrips(left_bank)
    elif boat == 'right':
        boat_trips = BoatTrips(right_bank)
    new_states = []
    for boat_trip in boat_trips:
        if boat == 'left':
            new_left_bank = left_bank - set(boat_trip)
            new_right_bank = right_bank | set(boat_trip)
        else:
            new_left_bank = left_bank | set(boat_trip)
            new_right_bank = right_bank - set(boat_trip)

        if check_valid(boat_trip, new_left_bank) and check_valid(boat_trip, new_right_bank):
            var1 = mesto('', new_left_bank, new_right_bank)
            var2 = mesto('', new_left_bank, new_right_bank)
            var3 = mesto('', new_left_bank, new_right_bank)
            var4 = mesto('', new_left_bank, new_right_bank)
            var5 = mesto('', new_left_bank, new_right_bank)
            var6 = mesto('', new_left_bank, new_right_bank)
            new_states.append((invert_boat(boat), var1, var2, var3, var4, var5, var6))
    return new_states

def BoatTrips(river_bank):
    boat_trips = []
    for passenger in river_bank:
        if passenger in sailors:
            boat_trips.append((passenger,))

    for possible_boat_trip in itertools.combinations(river_bank, 2):
        for sailor in sailors:
            if sailor in possible_boat_trip:
                boat_trips.append(possible_boat_trip)
    return boat_trips

def check_valid(boat_trip,river_bank):

    return True

def mesto(passenger, left_bank, right_bank):
    if passenger in left_bank:
        return 'left'
    elif passenger in right_bank:
        return 'right'

def invert_boat(side):
    if side == 'left':
        return 'right'
    return 'left'

passengers = []
sailors = []
initial_state = ()
goal_state = ()
path = search_path(initial_state, goal_state)
visualise(path)