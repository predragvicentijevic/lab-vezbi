import pandas as pd
from sklearn.datasets import load_iris
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.naive_bayes import BernoulliNB
from sklearn import tree
import numpy as np

def entropy(a, b):
    if a == 0 or b == 0:
        return 0
    m = a + b
    return - a/m * np.log2(a/m) - b/m * np.log2(b/m)

df = pd.DataFrame(
    [['дома', 'напаѓачка', 'сонце', 'одморени', 'да'],
     ['дома', 'одбранбена', 'сонце', 'одморени', 'да'],
     ['гости', 'одбранбена', 'сонце', 'одморени', 'да'],
     ['дома', 'одбранбена', 'сонце', 'изморени', 'да'],
     ['дома', 'одбранбена', 'дожд', 'одморени', 'да'],
     ['гости', 'одбранбена', 'дожд', 'изморени', 'не'],
     ['гости', 'напаѓачка', 'дожд', 'изморени', 'не'],
     ['гости', 'одбранбена', 'дожд', 'изморени', 'не'],
     ['гости', 'напаѓачка', 'дожд', 'одморени', 'не'],
     ['гости', 'одбранбена', 'сонце', 'изморени', 'не']], columns=['Терен', 'Тактика', 'Време', 'Одмореност', 'Победа'])
X=df[['Терен', 'Тактика', 'Време', 'Одмореност']].values
Y=df[['Победа']].values

for i in range(X.shape[0]):
    if X[i][0]=='дома':
        X[i][0]=1
    if X[i][0]=='гости':
        X[i][0]=0
for i in range(X.shape[0]):
    if X[i][1]=='напаѓачка':
        X[i][1]=1
    if X[i][1]=='одбранбена':
        X[i][1]=0
for i in range(X.shape[0]):
    if X[i][2]=='сонце':
        X[i][2]=1
    if X[i][2]=='дожд':
        X[i][2]=0
for i in range(X.shape[0]):
    if X[i][3]=='одморени':
        X[i][3]=1
    if X[i][3]=='изморени':
        X[i][3]=0
for i in range(Y.shape[0]):
    if Y[i][0]=='да':
        Y[i][0]=1
    if Y[i][0]=='не':
        Y[i][0]=0
X=X.astype(int)
Y=Y.astype(int)

X_train, X_test, Y_train, Y_test = train_test_split(X, Y, random_state = 155, test_size = 0.10)
clf = DecisionTreeClassifier(criterion = 'entropy')
clf.fit(X_train, Y_train)

tree.plot_tree(clf)
with open("zad2.txt", "w") as f:
    f = tree.export_graphviz(clf, out_file=f)

Y_pred =clf.predict(X_test)
print('Accuracy Score on train data: ', accuracy_score(y_true=Y_train, y_pred=clf.predict(X_train)))
print('Accuracy Score on test data: ', accuracy_score(y_true=Y_test, y_pred=Y_pred))

rezultat =clf.predict([[1,1,0,0]])
print('Drvo na razgranuvanje: ', rezultat)

clf = BernoulliNB(alpha=1.0)
clf.fit(X, Y.reshape(-1))
print('Naiven baesov klasifikator: ', clf.predict([[1,1,0,0]]))